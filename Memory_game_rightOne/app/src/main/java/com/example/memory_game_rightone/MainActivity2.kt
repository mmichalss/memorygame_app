package com.example.memory_game_rightone

        import android.os.Bundle
        import android.util.Log
        import android.widget.ImageButton
        import android.widget.Toast
        import androidx.appcompat.app.AppCompatActivity

private const val TAG = "MainActivity2"
        class MainActivity2 : AppCompatActivity() {

            private lateinit var buttons: List<ImageButton>
            private lateinit var cards: List<MemoryCard>
            private var indexOfSingleSelectedCard: Int? = null

            override fun onCreate(savedInstanceState: Bundle?) {
                super.onCreate(savedInstanceState)
                setContentView(R.layout.activity_main)

                val images = mutableListOf(R.drawable.garfield1, R.drawable.garfield2, R.drawable.garfield3,
                    R.drawable.garfield4, R.drawable.garfield5, R.drawable.garfield6)
                // Add each image twice so we can create pairs
                images.addAll(images)
                // Randomize the order of images
                images.shuffle()

                val button1 = findViewById<ImageButton>(R.id.button1)
                val button2 = findViewById<ImageButton>(R.id.button2)
                val button3 = findViewById<ImageButton>(R.id.button3)
                val button4 = findViewById<ImageButton>(R.id.button4)
                val button5 = findViewById<ImageButton>(R.id.button5)
                val button6 = findViewById<ImageButton>(R.id.button6)
                val button7 = findViewById<ImageButton>(R.id.button7)
                val button8 = findViewById<ImageButton>(R.id.button8)
                val button9 = findViewById<ImageButton>(R.id.button9)
                val button10 = findViewById<ImageButton>(R.id.button10)
                val button11 = findViewById<ImageButton>(R.id.button11)
                val button12 = findViewById<ImageButton>(R.id.button12)
                buttons = listOf(button1, button2, button3, button4, button5, button6,
                    button7, button8, button9, button10, button11, button12)

                cards = buttons.indices.map { index ->
                    MemoryCard(images[index])
                }

                buttons.forEachIndexed { index, button ->
                    button.setOnClickListener {
                        Log.i(TAG, "button clicked!!")
                        // Update models
                        updateModels(index)
                        // Update the UI for the game
                        updateViews()
                    }
                }
            }

            private fun updateViews() {
                cards.forEachIndexed { index, card ->
                    val button = buttons[index]
                    if (card.isMatched) {
                        button.alpha = 0.1f
                    }
                    button.setImageResource(if (card.isFaceUp) card.identifier else R.drawable.ic_code)
                }
            }

            private fun updateModels(position: Int) {
                val card = cards[position]
                // Error checking:
                if (card.isFaceUp) {
                    Toast.makeText(this, "Invalid move!", Toast.LENGTH_SHORT).show()
                    return
                }
                // Three cases
                // 0 cards previously flipped over => restore cards + flip over the selected card
                // 1 card previously flipped over => flip over the selected card + check if the images match
                // 2 cards previously flipped over => restore cards + flip over the selected card
                if (indexOfSingleSelectedCard == null) {
                    // 0 or 2 selected cards previously
                    restoreCards()
                    indexOfSingleSelectedCard = position
                } else {
                    // exactly 1 card was selected previously
                    checkForMatch(indexOfSingleSelectedCard!!, position)
                    indexOfSingleSelectedCard = null
                }
                card.isFaceUp = !card.isFaceUp
            }

            private fun restoreCards() {
                for (card in cards) {
                    if (!card.isMatched) {
                        card.isFaceUp = false
                    }
                }
            }

            private fun checkForMatch(position1: Int, position2: Int) {
                if (cards[position1].identifier == cards[position2].identifier) {
                    Toast.makeText(this, "Match found!!", Toast.LENGTH_SHORT).show()
                    cards[position1].isMatched = true
                    cards[position2].isMatched = true
                }
            }
        }