/*
package com.example.memory_game_rightone

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    private val buttonIds = arrayOf(
        R.id.button1, R.id.button2, R.id.button3, R.id.button4,
        R.id.button5, R.id.button6, R.id.button7, R.id.button8,
        R.id.button9, R.id.button10, R.id.button11, R.id.button12
    )

    private lateinit var buttons: List<Button>
    private lateinit var buttonValues: List<Int>
    private var firstClickedButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttons = buttonIds.map { findViewById<Button>(it) }
        buttons.forEach { button ->
            button.setOnClickListener {
                onButtonClick(button)
            }
        }

        //resetGame()
    }

    private fun resetGame() {
        val garfieldImageIds = mutableListOf(
            R.drawable.garfield1, R.drawable.garfield2,
            R.drawable.garfield3, R.drawable.garfield4,
            R.drawable.garfield5, R.drawable.garfield6
        )

        val imageIds = mutableListOf<Int>()
        repeat(buttons.size / 2) {
            val randomIndex = Random.nextInt(garfieldImageIds.size)
            val randomImage = garfieldImageIds.removeAt(randomIndex)
            imageIds.add(randomImage)
            imageIds.add(randomImage)
        }

        imageIds.shuffle()
        buttonValues = imageIds

        buttons.forEachIndexed { index, button ->
            setupButton(button, buttonValues[index])
        }
    }

    private fun setupButton(button: Button, value: Int) {
        with(button) {
            text = ""
            isEnabled = true
            setBackgroundResource(value)
            setOnClickListener {
                onButtonClick(this)
            }
        }
    }

    private fun onButtonClick(clickedButton: Button) {
        val clickedIndex = buttons.indexOf(clickedButton)
        val clickedValue = buttonValues[clickedIndex]

        clickedButton.text = clickedValue.toString()
        clickedButton.setBackgroundResource(android.R.drawable.btn_default)

        if (firstClickedButton == null) {
            firstClickedButton = clickedButton
        } else {
            handleSecondButtonClick(clickedButton, clickedValue)
        }

        if (buttonValues.all { !buttons[it].isEnabled }) {
            resetGame()
        }
    }

    private fun handleSecondButtonClick(clickedButton: Button, clickedValue: Int) {
        val firstClickedIndex = buttons.indexOf(firstClickedButton!!)
        val firstClickedValue = buttonValues[firstClickedIndex]

        if (clickedValue == firstClickedValue) {
            disableMatchedButtons(clickedButton, firstClickedButton!!)
        } else {
            resetMismatchedButtons(clickedButton, firstClickedButton!!)
        }

        firstClickedButton = null
    }

    private fun disableMatchedButtons(button1: Button, button2: Button) {
        button1.isEnabled = false
        button2.isEnabled = false
    }

    private fun resetMismatchedButtons(button1: Button, button2: Button) {
        button1.postDelayed({
            button1.text = ""
            button1.setBackgroundResource(android.R.drawable.btn_default)
            button2.text = ""
            button2.setBackgroundResource(android.R.drawable.btn_default)
        }, 1000)
    }
}
*/
